# fizzbuzz

Fizzbuzz CLI in Clojure!

## Installation

These instructions assume some understanding of lein. Check out [1] and [2] if you need a refresher!

```
git clone https://gitlab.com/hfish/clojure-fizzbuzz.git
cd clojure-fizzbuzz
lein uberjar
```

##### Optional - To make a simple executable, run:
```
echo '#!/usr/bin/java -jar ' > fizzbuzz
cat ./target/uberjar/fizzbuzz-0.0.1-standalone.jar >> fizzbuzz
```

## Usage
(These examples assumes the optional `fizzbuzz` executable was created. If not, use `java -jar ./target/uberjar/fizzbuzz-0.0.1-standalone.jar` in place of `fizzbuzz`)

```
./fizzbuzz --help
  -s, --start START  1    Start of fizzbuzz!
  -e, --end END      100  End of fizzbuzz!
  -h, --help
```

By default, prints fizzbuzz from 1 - 100:
```
./fizzbuzz | grep -n . | sed -n '1p;$p'
1:1
100:buzz
```

With the -s and -e flags, a range can be provided:
```
./fizzbuzz -s 100 -e 105
buzz
101
fizz
103
104
fizzbuzz
```

### Bugs
https://github.com/boot-clj/boot can be used for the shebang stuffs! Need to check it out.

## License
Intentionally left blank.

### Lein references
[1] http://leiningen.org/

[2] https://github.com/technomancy/leiningen/blob/master/doc/TUTORIAL.md for some great instructions.
