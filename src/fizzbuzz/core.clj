(ns fizzbuzz.core
  (:require [clojure.tools.cli :refer [parse-opts]])
  (:gen-class))

(def default-start 1)
(def default-end 100)

(defn fizz-buzz [n]
  ;Very simple fizzbuzz. 
  (condp #(zero? (mod %2 %1)) n
      15 "fizzbuzz"
      3  "fizz"
      5  "buzz"
      n))

(defn fizzbuzzes
  [options] 
  (let [start (:start options) 
        end (inc (:end options))]
    (->> (range start end)
         (filter #(not (zero? %))) ;skpping zero.
         (map fizz-buzz))))

(def cli-options 
  [["-s" "--start START" "Start of fizzbuzz!"
    :default default-start
    :parse-fn #(Integer/parseInt %)]
   ["-e" "--end END" "End of fizzbuzz!"
    :default default-end
    :parse-fn #(Integer/parseInt %)]
    ["-h" "--help"]])

(defn exit [status msg]
    (println msg)
    (System/exit status))

(defn -main  [& args]
  (let [{:keys [options arguments errors summary]} 
        (parse-opts args cli-options)]
    (cond errors (exit 1 errors) 
          (:help options) (exit 0 summary))
    (->> (fizzbuzzes options)
         (map println)
         doall)))
